procedure Dihotomy;
Var a0,b0,eps,delta,x1,y1,x2,y2,xmin:extended;
    i:integer;
begin
  eps:=sqrt(masheps);
  delta:=eps/2;
  i:=0;
  a0:=0;
  b0:=1;
  writeln;
  write('***Dihotomy***');
  writeln;
 // writeln(eps);
 // writeln(delta);
 while abs(a0-b0)>eps do
  begin
    x1:=(a0+b0-delta)/2;
    x2:=(a0+b0+delta)/2;
    y1:=z(x1);
    y2:=z(x2);
    if y1<y2 then b0:=x2
    else a0:=x1;
    i:=i+1;
  end;
  xmin:=(a0+b0)/2;
  write('Xmin: ');
  write(xmin);
  writeln;
  write('f(Xmin): ');
  write(z(xmin));
  writeln;
  write('Count: ');
  write(i);
  writeln;
end;
