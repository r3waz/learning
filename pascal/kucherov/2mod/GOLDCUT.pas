procedure GoldCut;
Var a0,b0,eps,x1,x2,y1,y2,F,xmin:extended;
    i:integer;
begin
  a0:=0;
  b0:=1;
  eps:=sqrt(masheps);
  i:=1;
  F:=(sqrt(5)+1)/2;
  writeln;
  write('***Goldcut***');
  writeln;
  x1:=a0+(2-F)*(b0-a0);
  x2:=a0+(F-1)*(b0-a0);
  y1:=z(x1);
  y2:=z(x2);
  while abs(a0-b0)>eps do
  begin
    if y1<y2 then
    begin
      b0:=x2;
      x2:=x1;
      y2:=y1;
      x1:=a0+(2-F)*(b0-a0);
      y1:=z(x1);
    end
    else
    begin
      a0:=x1;
      x1:=x2;
      y1:=y2;
      x2:=a0+(F-1)*(b0-a0);
      y2:=z(x2);
    end;
    i:=i+1;
  end;
  xmin:=(a0+b0)/2;
  write('Xmin: ');
  write(xmin);
  writeln;
  write('f(Xmin): ');
  write(z(xmin));
  writeln;
  write('Count: ');
  write(i);
  writeln;
end;
