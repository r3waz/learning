function HalfCut:extended;
Var a0,b0,eps,xs,l,x1,x2,ys,y1,y2,xmin:extended;
    i:integer;
begin
  a0:=0;
  b0:=5;
  eps:=sqrt(masheps);
  i:=1;
  writeln;
  write('***Halfcut***');
  writeln;
  l:=abs(a0-b0);
  xs:=(a0+b0)/2;
  while l>eps do
  begin
    x1:=a0+l/4;
    x2:=b0-l/4;
    ys:=z(xs);
    y1:=z(x1);
    y2:=z(x2);
    if y1<ys then
    begin
      b0:=xs;
      xs:=x1
    end
    else if y2<ys then
         begin
           a0:=xs;
           xs:=x2;
         end
         else
         begin
           a0:=x1;
           b0:=x2;
         end;
    i:=i+1;
    l:=abs(a0-b0);
  end;
  xmin:=(a0+b0)/2;
  write('Xmin: ');
  write(xmin);
  writeln;
  write('f(Xmin): ');
  write(z(xmin));
  writeln;
  write('Count: ');
  write(i);
  writeln;
  HalfCut:=xmin;
end;
