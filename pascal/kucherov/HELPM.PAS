Unit HelpM;
{$E-}{$N+}
Interface


Uses Graph,Dos;

            Const
   GridGraphik   : Boolean   = FALSE         ;

Procedure Starting(k:Integer;j:integer;s0:String);

Type
  mass=Array [1..6] of extended;
  matr= Array [1..6,1..6] of Extended;

  Graphic = Object
            x1,y1,x2,y2                 { ��������� ����           }
                                        :Word                      ;
            BackGround,Color            :Byte                      ;
            Xmin,Xmax,Ymin,Ymax         { ��������� � ���ᨬ���- }
                                        { �� ���祭�� ����稭 ��- }
                                        { ������� �� ��䨪        }
                                        :Extended                  ;
            MashtX,MashtY               { ����⠡�� �����樥���  }
                                        :Extended                  ;
            TabX,TabY,Seredina          :Word                      ;
            Vision                      :Boolean                   ;
            ParamOld                    :Boolean                   ;
            PointFunc                   :Boolean                   ;
            ShowInfo                    :Boolean                   ;
            FlagMoving                  :Boolean                   ;
            StepMoving                  :Extended                  ;
{-1-}   Constructor  Init                                          ;
{-2-}   Procedure    SetWindow(X1new,Y1new,X2new,Y2new:Word)       ;
{-3-}   Procedure    Draw                                          ;
{-4-}   Procedure    DrawKoord                                     ;
{-6-}   Procedure    Show                                          ;
{-7-}   Procedure    Hide                                          ;
{-8-}   Procedure    Clear                                         ;
{-9-}   Procedure    CalcMasht                                     ;
{-10-}  Function     GetX (x:Extended)             :Word           ;
{-11-}  Function     GetY (y:Extended)             :Word           ;
{-12-}  Procedure    SetXYRange(t1,t2,t3,t4:Extended)              ;
{-13-}  Procedure    SetColor(c,f:Byte)                            ;
{-14-}  Function     GetRealX(x:LongInt)           :Extended       ;
{-15-}  Function     GetRealY(y:LongInt)           :Extended       ;
{-18-}  Procedure    PutPixel(x,y:Extended)                        ;
  End;


real=extended;
     GraficType = Record
       x1,y1,x2,y2,TabX,TabY,Seredina:Word;
       MinX,MinY,MaxX,MaxY,MashtX,MashtY:Extended;
     End;

PROCEDURE GAUSS(a:matr;b:mass;var x:mass;n:integer);
PROCEDURE DTM(a:matr;var dt:real;n:integer);
procedure mm(a,b:matr; var c:matr;n:integer);
Procedure mb(a:matr; y:mass; var x:mass;n:integer);
Procedure OM(a:Matr;Var x1:matr;n:integer);
PROCEDURE LEQ_(f:matr;var a:mass;n:integer);
PROCEDURE LEOQM(f:matr;var rr,rm:mass;n:integer);
PROCEDURE ZM(a:matr;h:extended;var om,ExpMatr:matr;n:integer);
Procedure KorPm(A1:Mass;Var Re,Im:Mass;n:integer);
{Procedure WriteScreenInFile(Name:String);}
Implementation

Uses Crt;

Const GraficColor:Byte=15;
      GraficBkColor:Byte=0;

  TabLeft   = 20    ;
  TabRight  = 10    ;
  TabUp     = 40    ;
  TabDown   = 20    ;


Var ViewPortGrafic:ViewPortType;
    MashtX,MashtY:Real;
    Tab_X,Tab_Y,Seredina:Word;
    DigitMinX,DigitMaxX,DigitMinY,DigitMaxY:Real;


  PROCEDURE GAUSS(a:matr;b:mass;var x:mass;n:integer);
            label 1;
            var i,k,j,l:integer;
                r,v:real;
                a2:matr;
      begin
          for i:=1 to n do
          begin
          for j:=1 to n do
                a2[i,j]:=a[i,j]
                end;
{***************************************************}
          for l:=1 to n-1 do
          {*****************}
           begin
           r:=a2[l,l];
                k:=l;
           for i:=l+1 to n do  begin
                if abs(a2[i,l])>abs(r) then
            begin
                r:=a2[i,l];
                k:=i
            end
            end;
            if k=l then goto 1;
                r:=b[l]; b[l]:=b[k];    b[k]:=r;
            for i:=l to n do
            begin
            r:=a2[l,i];
            a2[l,i]:=a2[k,i];
            a2[k,i]:=r
            end;
     {***********************************}
1:          for i:=l+1 to n do
               begin
               r:=a2[i,l]/a2[l,l];
               b[i]:=b[i]-b[l]*r;
               for j:=l to n do
               a2[i,j]:=a2[i,j]-a2[l,j]*r
               end
               end;   {*l*}
               for i:=n downto 1 do  begin
               x[i]:=b[i];
               for j:=n downto i+1 do
               x[i]:=x[i]-a2[i,j]*x[j];
               x[i]:=x[i]/a2[i,i]
               end
               end;

  PROCEDURE DTM(a:matr;var dt:real;n:integer);
            label 1;
            var i,k,j,l:integer;
                d:real;
           begin
{***************************************************}
          dt:=1;
          for k:=1 to n-1 do
          {*****************}
           begin
                i:=k;
           for j:=k+1 to n do
                begin
                if abs(a[j,k])>abs(a[i,k]) then
                i:=j;
                end;
            if i<>k then
            begin
           dt:=-dt;
            for j:=k to n do
            begin
            d:=a[k,j];
            a[k,j]:=a[i,j];
            a[i,j]:=d;
            end;
            end;
     {***********************************}
           if a[k,k]=0 then
           begin
           dt:=0;
           goto 1;
           end;
          for i:=k+1 to n do
               begin
               d:=a[i,k]/a[k,k];
               for j:=k to n do
               begin
               if d<>0 then
               a[i,j]:=a[i,j]-d*a[k,j];
               end;
               end;
               end;   {*k*}
               for i:=1 to n do
               dt:=dt*a[i,i];
            1: end;

  procedure mm(a,b:matr; var c:matr;n:integer);
            var i,k,j:integer;
            sum:real;
      begin
        for i:=1 to n do
            begin
            for k:=1 to n do
                begin
                sum:=0;
                for j:=1 to n do
                sum:=sum+a[i,j]*b[j,k];
                c[i,k]:=sum
                end
            end
       end;

Procedure mb(a:matr; y:mass; var x:mass;n:integer);
      var i,j:integer;
          s:real;
          begin
          for j:=1 to n do
              begin
              s:=0;
              for i:=1 to n do s:=s+a[j,i]*y[i];
              x[j]:=s
              end
           end;

Procedure OM(a:Matr;Var x1:matr;n:integer);
      var   p,c:matr;
           j,k,i:integer;
           s,v:real;
           b,x:mass;
           begin
           for j:=1 to n do
           b[j]:=0;
           for i:=1 to n do
           begin
           b[i]:=1;
           GAUSS(a,b,x,n);
          for k:=1 to n do
          x1[k,i]:=x[k];
         for j:=1 to n do
              b[j]:=0;
              end;
              end;
(************)

  PROCEDURE LEQ_(f:matr;var a:mass;n:integer);
  {��楤�� ���� �ࠪ�����᪮�� �����童��}
  {f - �����⭠� ����� ࠧ��୮�� n x n; a -
   ���ᨢ �����. �����童�� ࠧ��୮�� n x 1;
   ���ᠭ�� ⨯� ��६����� f: matr=array[1..n,1..n] of real;}
  { Copyright (c) 1992 by Zhusubaliev Zh.T. }
          Var i,k,j:integer;
              bk,lk:real;
              d,f1:matr;
              b:mass;
       Function Sgn(x:integer):Extended;
        Begin
        If Odd(x) then
           Sgn:=-1
           else  Sgn:=1
        end;
       PROCEDURE DTM(a:matr;var dt:real;n:integer);
          label 1;
          var i,k,j,l:integer;
              d:real;
           begin
             dt:=1;
             for k:=1 to n-1 do
                 begin
                   i:=k;
                   for j:=k+1 to n do
                     begin
                      if abs(a[j,k])>abs(a[i,k]) then
                      i:=j
                     end;
                  if i<>k then
                     begin
                       dt:=-dt;
                       for j:=k to n do
                         begin
                           d:=a[k,j];
                           a[k,j]:=a[i,j];
                           a[i,j]:=d
                         end
                     end;
                 if a[k,k]=0 then
                    begin
                      dt:=0;
                      goto 1
                    end;
                for i:=k+1 to n do
                    begin
                      d:=a[i,k]/a[k,k];
                      for j:=k to n do
                          begin
                            if d<>0 then
                            a[i,j]:=a[i,j]-d*a[k,j]
                          end
                    end
                 end;   {*k*}
                    for i:=1 to n do
                        dt:=dt*a[i,i];
           1: end;
   PROCEDURE GAUSS(a:matr;b:mass;var x:mass;n:integer);
            label 1;
            var i,k,j,l:integer;
                r,v:real;
                a2:matr;
      begin
          for i:=1 to n do
          begin
          for j:=1 to n do
                a2[i,j]:=a[i,j]
                end;
          for l:=1 to n-1 do
           begin
           r:=a2[l,l];
                k:=l;
           for i:=l+1 to n do
                begin
                if abs(a2[i,l])>abs(r) then
            begin
                r:=a2[i,l];
                k:=i
            end
            end;
            if k=l then goto 1;
                r:=b[l]; b[l]:=b[k];    b[k]:=r;
            for i:=l to n do
            begin
            r:=a2[l,i];
            a2[l,i]:=a2[k,i];
            a2[k,i]:=r
            end;
1:          for i:=l+1 to n do
               begin
               r:=a2[i,l]/a2[l,l];
               b[i]:=b[i]-b[l]*r;
               for j:=l to n do
               a2[i,j]:=a2[i,j]-a2[l,j]*r;
               end;
               end;   {*l*}
               for i:=n downto 1 do  begin
               x[i]:=b[i];
               for j:=n downto i+1 do
               x[i]:=x[i]-a2[i,j]*x[j];
               x[i]:=x[i]/a2[i,i]
               end
               end;
  {----------------------------------------------------------}
         BEGIN
              for k:=1 to n do
                  begin
                    d[k,n]:=1;
                    lk:=k/(n+1);
                    for i:=1 to n-1 do
                    d[k,n-i]:=d[k,n-i+1]*lk;
                    for i:=1 to n do
                       begin
                         for j:=1 to n do
                             begin
                               if i=j then  f1[i,j]:=f[i,j]-lk
                               else  f1[i,j]:=f[i,j]
                            end { j }
                       end;{ i }
                    DTM(f1,bk,n);
                    b[k]:=bk-Sgn(n)*lk*d[k,1]
                  end;{ k }
                  GAUSS(d,b,a,n);
         if Odd(n)
         then for k:=1 to n do
         a[k]:=-a[k]
         END;{ leq }
{=======================================================================}
  PROCEDURE LEOQM(f:matr;var rr,rm:mass;n:integer);
  {��楤�� ���� �ࠪ�����᪮�� �����童��}
  {f - �����⭠� ����� ࠧ��୮�� n x n; a -
   ���ᨢ �����. �����童�� ࠧ��୮�� n x 1;
   ���ᠭ�� ⨯� ��६����� f: matr=array[1..n,1..n] of real;}
  { Copyright (c) 1994 by Zhusubaliev Zh.T. }
          Var i,k,j:integer;
              bk,lk:real;
              d,f1:matr;
              b,bp:mass;
       Function Sgn(x:integer):Extended;
        Begin
        If Odd(x) then
           Sgn:=-1
           else  Sgn:=1
        end;
       PROCEDURE DTM(a:matr;var dt:real;n:integer);
          label 1;
          var i,k,j,l:integer;
              d:real;
           begin
             dt:=1;
             for k:=1 to n-1 do
                 begin
                   i:=k;
                   for j:=k+1 to n do
                     begin
                      if abs(a[j,k])>abs(a[i,k]) then
                      i:=j
                     end;
                  if i<>k then
                     begin
                       dt:=-dt;
                       for j:=k to n do
                         begin
                           d:=a[k,j];
                           a[k,j]:=a[i,j];
                           a[i,j]:=d
                         end
                     end;
                 if a[k,k]=0 then
                    begin
                      dt:=0;
                      goto 1
                    end;
                for i:=k+1 to n do
                    begin
                      d:=a[i,k]/a[k,k];
                      for j:=k to n do
                          begin
                            if d<>0 then
                            a[i,j]:=a[i,j]-d*a[k,j]
                          end
                    end
                 end;   {*k*}
                    for i:=1 to n do
                        dt:=dt*a[i,i];
           1: end;
   PROCEDURE GAUSS(a:matr;b:mass;var x:mass;n:integer);
            label 1;
            var i,k,j,l:integer;
                r,v:real;
                a2:matr;
      begin
          for i:=1 to n do
          begin
          for j:=1 to n do
                a2[i,j]:=a[i,j]
                end;
          for l:=1 to n-1 do
           begin
           r:=a2[l,l];
                k:=l;
           for i:=l+1 to n do
                begin
                if abs(a2[i,l])>abs(r) then
            begin
                r:=a2[i,l];
                k:=i
            end
            end;
            if k=l then goto 1;
                r:=b[l]; b[l]:=b[k];    b[k]:=r;
            for i:=l to n do
            begin
            r:=a2[l,i];
            a2[l,i]:=a2[k,i];
            a2[k,i]:=r
            end;
1:          for i:=l+1 to n do
               begin
               r:=a2[i,l]/a2[l,l];
               b[i]:=b[i]-b[l]*r;
               for j:=l to n do
               a2[i,j]:=a2[i,j]-a2[l,j]*r;
               end;
               end;   {*l*}
               for i:=n downto 1 do  begin
               x[i]:=b[i];
               for j:=n downto i+1 do
               x[i]:=x[i]-a2[i,j]*x[j];
               x[i]:=x[i]/a2[i,i]
               end
               end;
  {==========================================================}
   Procedure KornPol(A1:Mass;Var Re,Im:Mass;N:integer);
  label m1;
  Const
       eps:extended=1e-11;
  Var
     P,Q,dP,dQ,D:extended;
     A,B,C:Mass;
     Step_s,i,j,ik:integer;
 Function Sign(X:extended):extended;
  Begin
   If X<0 Then Sign:=-1
          Else Sign:=1;
  End;

 Procedure Korn2;
  Begin
   Re[iK]:=-P/2;
   Re[iK+1]:=Re[iK];
   D:=Sqr(P)/4-Q;
   If D>0 Then Begin

    Re[iK]:=-P/2+Sign(P)*Sqrt(D);
    Re[iK+1]:=-P/2-Sign(P)*Sqrt(D);
    Im[iK]:=0;
    Im[iK+1]:=0;
   End
   Else Begin
    Im[iK]:=Sqrt(-D);
    Im[iK+1]:=-Im[iK];
   End;
  End;


  Begin
   a[1]:=1;
   for i:=2 to n+1 do
   a[i]:=a1[i-1];
   iK:=1;
   if n<=2 Then goto m1;
   Repeat
    P:=A[2];Q:=A[3]; Step_s:=0;
    Repeat
     B[1]:=A[1];B[2]:=A[2]-P*B[1];
     For i:=3 to N+1 Do B[i]:=A[i]-P*B[i-1]-Q*B[i-2];
     C[1]:=B[1];C[2]:=B[2]-P*C[1];
     For i:=3 to N-1 Do C[i]:=B[i]-P*C[i-1]-Q*C[i-2];
     C[N]:=-P*C[N-1]-Q*C[N-2];
     D:=(Sqr(C[N-1])-C[N]*C[N-2]);
     dP:=(B[N]*C[N-1]-B[N+1]*C[N-2])/d;
     dQ:=(B[N+1]*C[N-1]-B[N]*C[N])/d;
     P:=P+dP;
     Q:=Q+dQ;
     Step_s:=Step_s+1;
     If Step_s>800 Then Exit;
    Until (Abs(dP)<Eps) And (Abs(dQ)<Eps);
    Korn2;
    Inc(iK,2);
    Dec(N,2);
    For i:=1 to N+1 Do A[i]:=B[i];
   Until N<3;
m1:
   Case N Of
    1:Begin
       Re[iK]:=-A[2]/A[1];
       Im[iK]:=0;
      End;
    2:Begin
       P:=A[2];Q:=A[3];
       Korn2;
      End
      Else
   End;
  End;
  {==========================================================}
         BEGIN
              for k:=1 to n do
                  begin
                    d[k,n]:=1;
                    lk:=k/(n+1);
                    for i:=1 to n-1 do
                    d[k,n-i]:=d[k,n-i+1]*lk;
                    for i:=1 to n do
                       begin
                         for j:=1 to n do
                             begin
                               if i=j then  f1[i,j]:=f[i,j]-lk
                               else  f1[i,j]:=f[i,j]
                            end { j }
                       end;{ i }
                    DTM(f1,bk,n);
                    b[k]:=bk-Sgn(n)*lk*d[k,1]
                  end;{ k }
                  GAUSS(d,b,bp,n);
         if Odd(n)
         then for k:=1 to n do
         bp[k]:=-bp[k];
         Kornpol(bp,rr,rm,n);
         END;{ leq }
{------------------------------------------------------------------------}
 Procedure KorPm(A1:Mass;Var Re,Im:Mass;N:integer);
  label m1;
  Const
       eps:extended=1e-11;
  Var
     P,Q,dP,dQ,D:extended;
     A,B,C:Mass;
     i,j,ik:integer;
 Function Sign(X:extended):extended;
  Begin
   If X<0 Then Sign:=-1
          Else Sign:=1;
  End;

 Procedure Korn2;
  Begin
   Re[iK]:=-P/2;
   Re[iK+1]:=Re[iK];
   D:=Sqr(P)/4-Q;
   If D>0 Then Begin

    Re[iK]:=-P/2+Sign(P)*Sqrt(D);
    Re[iK+1]:=Q/Re[iK];
    Im[iK]:=0;
    Im[iK+1]:=0;
   End
   Else Begin
    Im[iK]:=Sqrt(-D);
    Im[iK+1]:=-Im[iK];
   End;
  End;


  Begin
   a[1]:=1;
   for i:=2 to n+1 do
   a[i]:=a1[i-1];
   iK:=1;
   if n<=2 Then goto m1;
   Repeat
    P:=A[2];Q:=A[3];
    Repeat
     B[1]:=A[1];B[2]:=A[2]-P*B[1];
     For i:=3 to N+1 Do B[i]:=A[i]-P*B[i-1]-Q*B[i-2];
     C[1]:=B[1];C[2]:=B[2]-P*C[1];
     For i:=3 to N-1 Do C[i]:=B[i]-P*C[i-1]-Q*C[i-2];
     C[N]:=-P*C[N-1]-Q*C[N-2];
     D:=(Sqr(C[N-1])-C[N]*C[N-2]);
     dP:=(B[N]*C[N-1]-B[N+1]*C[N-2])/d;
     dQ:=(B[N+1]*C[N-1]-B[N]*C[N])/d;
     P:=P+dP;
     Q:=Q+dQ;
    Until (Abs(dP)<Eps) And (Abs(dQ)<Eps);
    Korn2;
    Inc(iK,2);
    Dec(N,2);
    For i:=1 to N+1 Do A[i]:=B[i];
   Until N<3;
m1:
   Case N Of
    1:Begin
       Re[iK]:=-A[2]/A[1];
       Im[iK]:=0;
      End;
    2:Begin
       P:=A[2];Q:=A[3];
       Korn2;
      End
      Else
   End;
  End;
{========================================================================}
Procedure Starting(k:Integer;j:integer;s0:String);
{Var GraphDriver,GraphMode,ErrorCode:Integer;}
Begin
{ DetectGraph(GraphDriver,GraphMode);
 InitGraph(GraphDriver,GraphMode,s0);
 ErrorCode:=GraphResult;
 If ErrorCode<>0 Then Writeln ('�訡��');}
  InitGraph(k,j,s0);
End;



                   {-1-}

 Constructor Graphic.Init;
 Begin
   X1:=0; Y1:=0; X2:=GetMaxX; Y2:=GetMaxY;
   Vision:=False;
   Xmin:=0; Xmax:=1;
   Ymin:=-1; Ymax:=1;
   BackGround:=0; Color:=15;
   PointFunc:=False;
   ParamOld:=True;
{   FlagMoving:=False;
   StepMoving:=50;
   ShowInfo:=True;}
   CalcMasht;
 End;

                   {-2-}

 Procedure Graphic.SetWindow(X1new,Y1new,X2new,Y2new:Word);
 Begin
   If ((X1<>X1new)Or(X2<>X2new)Or
      (Y1<>Y1New)Or(Y2<>Y2new))And
      (X2new-X1new>45)And(Y2new-Y1new>48) Then Begin
     X1:=X1new; X2:=X2new;
     Y1:=Y1new; Y2:=Y2new;
     ParamOld:=True;
     CalcMasht;
     Draw;
   End;
 End;

                   {-3-}

 Procedure Graphic.Draw;
 Var
   Param        :FillSettingsType;
   Cvet         :Byte            ;
 Begin
   If Vision And ParamOld Then Begin
     GetFillSettings(Param);
     Cvet:=GetColor;

     SetFillStyle(1,BackGround);
     Graph.SetColor(Color);
     Bar(x1,y1,x2,y2);

     Rectangle(x1,y1,x2,y2);

{     If ShowInfo Then DrawHelpInfo;}
     DrawKoord;
     CalcMasht;

     Graph.SetColor(Cvet);
     SetFillStyle(Param.Pattern,Param.Color);

     ParamOld:=False;
   End;
 End;

                   {-4-}

 {Procedure Graphic.DrawKoord;
 Var
   StepX,StepY      :LongInt         ;
   i                :Byte            ;
   si0              :String          ;
 Begin

   StepX:=Round((x2-45-x1)/10);
   StepY:=Round((y2-48-y1)/10);

   Line(x1+20,y2-11,x1+20,y1+20);
   Line(x1+20,y1+20,x1+22,y1+30);
   Line(x1+20,y1+20,x1+18,y1+30);
   Line(x1+18,y2-12,x2-10,y2-12);
   Line(x2-10,y2-12,x2-20,y2-14);
   Line(x2-10,y2-12,x2-20,y2-10);


   For i:=1 to 10 do Begin
     MoveTo(x1+20+i*StepX,y2-11);LineTo(x1+20+i*StepX,y2-13);
     MoveTo(x1+18+i*StepX,y2-9);
     Str(i,si0);
     If ShowInfo Then OutText(si0);
     MoveTo(x1+19,y2-12-i*StepY);
     LineTo(x1+21,y2-12-i*StepY);
     MoveTo(x1+3,y2-15-i*StepY);
     Str(i,si0);
     If ShowInfo Then OutText(si0);
   End;

   If ShowInfo Then Begin
   MoveTo(x2-10,y2-9); OutText('X');
   MoveTo(x1+3,y1+22); OutText('Y');
   MoveTo(x1+3,y2-15); OutText('0');
   MoveTo(x1+17,y2-9); OutText('0');
   End;

 End;}

  Procedure Graphic.DrawKoord;
 Var
   StepX,StepY      :Extended        ;
   i                :Byte            ;
   si0              :String          ;
 Begin
   CalcMasht;

   StepX:=(Xmax-Xmin)/10;
   StepY:=(Ymax-Ymin)/10;

   Line(x1+TabLeft,y2-TabDown+1,x1+TabLeft,y1+TabUp);
   Line(x1+TabLeft,y1+TabUp,x1+TabLeft+2,y1+TabUp+10);
   Line(x1+TabLeft,y1+TabUp,x1+TabLeft-2,y1+TabUp+10);
   Line(x1+TabLeft-2,y2-TabDown,x2-TabRight,y2-TabDown);
   Line(x2-TabRight,y2-TabDown,x2-TabRight-10,y2-TabDown-2);
   Line(x2-TabRight,y2-TabDown,x2-TabRight-10,y2-TabDown+2);

   For i:=1 to 10 do Begin
    If GridGraphik Then Begin
      MoveTo(GetX(Xmin+StepX*i),y2-TabDown+2);
      LineTo(GetX(Xmin+StepX*i),GetY(Ymax));
    End
    Else Begin
      MoveTo(GetX(Xmin+StepX*i),y2-TabDown+2);
      LineTo(GetX(Xmin+StepX*i),y2-TabDown-2);
    End;
    Str(i,si0);OutTextXY(GetX(Xmin+StepX*i)-3,y2-TabDown+2,si0);
    If GridGraphik Then Begin
      MoveTo(x1+TabLeft-2,GetY(Ymin+i*StepY));
      LineTo(GetX(Xmax),GetY(Ymin+i*StepY));
    End
    Else Begin
      MoveTo(x1+TabLeft-2,GetY(Ymin+i*StepY));
      LineTo(x1+TabLeft+2,GetY(Ymin+i*StepY));
    End;
    Str(i,si0);OutTextXY(x1+TabLeft-17,GetY(Ymin+i*StepY)-5,si0);
  End;

  OutTextXY(x2-TabRight,y2-TabDown+3,'X');
  OutTextXY(x1+TabLeft-17,y1+TabUp-2,'Y');
  OutTextXY(x1+TabLeft-17,y2-TabDown-3,'0');
  OutTextXY(x1+TabLeft-3,y2-TabDown+3,'0');

 End;


                   {-5-}

{ Procedure Graphic.DrawHelpInfo;
 Begin
   MoveTo(x1+Trunc((x2-x1)/2)+3,y1+11);
   OutText(Concat('X10=',NtoC(Xmax,Trunc((x2-x1-34)/(2*9)))));
   MoveTo(x1+Trunc((x2-x1)/2)+3,y1+3);
   outText(Concat('X0 =',NtoC(Xmin,Trunc((x2-x1-34)/(2*9)))));
   MoveTo(x1+3,y1+11);
   OutText(Concat('Y10=',NtoC(Ymax,Trunc((x2-x1-34)/(2*9)))));
   MoveTo(x1+3,y1+3);
   OutText(Concat('Y0 =',NtoC(Ymin,Trunc((x2-x1-34)/(2*9)))));
 End;
}
                   {-6-}

 Procedure Graphic.Show;
 Begin
   If Not(Vision) Then Begin
     ParamOld:=True;
     Vision:=True;
     Draw;
   End;
 End;

                   {-7-}

 Procedure Graphic.Hide;
 Begin
   Vision:=False;
 End;

                   {-8-}

 Procedure Graphic.Clear;
 Begin
   ParamOld:=True;
   Draw;
 End;

                   {-9-}

 Procedure Graphic.CalcMasht;
 Var
   Setka        :Extended   ;
 Begin
   Setka:=x2-x1-45;
   MashtX:=Setka/(Xmax-Xmin);
   Setka:=y2-y1-48;
   MashtY:=Setka/(Ymax-Ymin);
   TabX:=x1+19; TabY:=y1+35;
   Seredina:=y1+36+((y2-y1-48) div 2);
 End;

                   {-10-}

 Function Graphic.GetX(x:Extended):Word;
 Var
   Xnew      :Extended   ;
 Begin
   Xnew:=(x-Xmin)*MashtX+TabX;
   If (Xnew<x1)Or(Xnew>x2) Then Begin
     If Xnew<x1 Then GetX:=X1
                Else GetX:=X2;
   End
   Else GetX:=Word(Round(Xnew));
 End;

                   {-11-}

 Function Graphic.GetY(y:Extended):Word;
 Var
   Ynew      :Extended   ;
 Begin
   Ynew:=(y-Ymin)*MashtY+TabY;
   Ynew:=Ynew-(Ynew-Seredina)*2;
   If (Ynew<Y1)Or(Ynew>Y2) Then Begin
     If Ynew<Y1 Then GetY:=Y1
     Else GetY:=Y2;
   End
   Else Begin
     GetY:=Word(Round(Ynew));
   End;
 End;

                   {-12-}

 Procedure Graphic.SetXYRange(t1,t2,t3,t4:Extended);
 Begin
   If (Xmin<>t1)Or(Xmax<>t2)Or
      (Ymin<>t3)Or(Ymax<>t4) Then Begin
     ParamOld:=True;
     Xmin:=t1; Xmax:=t2;
     Ymin:=t3; Ymax:=t4;
     CalcMasht;
     Draw;
   End;
 End;

                   {-13-}

 Procedure Graphic.SetColor(c,f:Byte);
 Begin
   BackGround:=f;
   Color:=c;
   ParamOld:=True;
   Draw;
 End;

                   {-14-}

 Function Graphic.GetRealX(X:LongInt):Extended;
 Begin
   GetRealX:=(X-TabX)/MashtX+Xmin;
 End;

                   {-15-}

 Function Graphic.GetRealY(Y:LongInt):Extended;
 Var
   Ynew    :Extended    ;
 Begin
   Ynew:=y-(y-Seredina)*2;
   GetRealY:=(Ynew-TabY)/MashtY+Ymin;
 End;

                   {-16-}

 {Procedure Graphic.InitFunc(p:Pointer);
 Begin
   PointFunc:=True;
   Func:=Func2D(p);
 End;}

                   {-17-}

 {Procedure Graphic.DrawFunc;
 Var
   x,dx     :Extended    ;
 Begin
   If PointFunc Then Begin
     x:=Xmin; dx:=(Xmax-Xmin)/(x2-x1);
     While x<=Xmax do Begin
       PutPixel(x,Func(x));
       x:=x+dx;
     End;
   End;
 End;}

                   {-18-}

 Procedure Graphic.PutPixel(x,y:Extended);
 Begin
{   If GetX(x)>=x2 Then Moving;}
   Graph.PutPixel(GetX(x),GetY(y),GetColor);
 End;

                   {-19-}



                   {-22-}



PROCEDURE ZM(a:matr;h:extended;var om,ExpMatr:matr;n:integer);
      var   s,i,j,k:integer;
            nm,sk,sk1,st,x:extended;
            e,e2,sm,z,z1:matr;

  procedure Ematr(var e:matr;n:integer);
            var i,j:integer;
      begin
            for i:=1 to n do
            for j:=1 to n do
                if i=j then
                e[i,j]:=1 else
                e[i,j]:=0
       end;

  procedure Norma(a:matr; var nm:extended;n:integer);
            var i,j,k,s:integer;
            sum:extended;
      begin
            sum:=0;
            for i:=1 to n do
            for j:=1 to n do
                sum:=sum+a[i,j]*a[i,j];
                nm:=sqrt(sum)
       end;
       procedure mm(a,b:matr; var c:matr;n:integer);
            var i,k,j:integer;
            sum:real;
      begin
        for i:=1 to n do
            begin
            for k:=1 to n do
                begin
                sum:=0;
                for j:=1 to n do
                sum:=sum+a[i,j]*b[j,k];
                c[i,k]:=sum
                end
            end
       end;
Procedure SumMatr(a,b:matr; var c:matr; n:integer);
      Var i,j:integer;
          begin
            for i:=1 to n do
            for j:=1 to n do
            c[i,j]:=a[i,j]+b[i,j]
           end;
      Procedure MatrSk(a:matr;sk:extended;var c:matr; n:integer);
      Var i,j:integer;
          begin
            for i:=1 to n do
            for j:=1 to n do
            c[i,j]:=a[i,j]*sk
           end;
(*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
      BEGIN
         Norma(a,nm,n);
         if (h=0)or(nm=0) then s:=1 else
         begin
         x:= ln(10*nm*h)/ln(2);
         s:=Round(x);
         if s<1 then s:=1;
         end;
         st:=EXP(s*ln(2));
         Ematr(e,n);
         MatrSk(e,2,e2,n);
         z:=e;
         sm:=e;
    (*----��� W(A,h/2s)---------*)
     for i:=1 to s do
          begin
           MM(a,z,z1,n);
           sk:=h/st/(i+1);
           MatrSk(z1,sk,z,n);
           SumMatr(sm,z,sm,n);
          end;
           sk1:=h/st;
           MatrSk(sm,sk1,z,n);
    (*----१���. W(A,h/2s)------*)
    (*============================*)
    (*----���   W(A,h)-----------*)
      for k:=1 to s do
          begin
           MM(a,z,sm,n);
           SumMatr(e2,sm,sm,n);
           MM(z,sm,z,n);
         end;
     (*----१���. W(A,h)------*)
(*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
     om:=z;
     MM(a,z,z,n);
     SumMatr(e,z,ExpMatr,n);
     END;(*ZM*)


End.
