Unit HelpGraph;


Interface

Const
   GridGraphik   : Boolean   = FALSE         ;

Procedure Starting(k:Integer;j:integer;s0:String);


Type


  Graphic = Object
            x1,y1,x2,y2                 { ��������� ����           }
                                        :LongInt                   ;
            BackGround,Color            :Byte                      ;
            Xmin,Xmax,Ymin,Ymax         { ��������� � ���ᨬ���- }
                                        { �� ���祭�� ����稭 ��- }
                                        { ������� �� ��䨪        }
                                        :Extended                  ;
            MashtX,MashtY               { ����⠡�� �����樥���  }
                                        :Extended                  ;
            TabX,TabY,Seredina          :LongInt                      ;
            Vision                      :Boolean                   ;
            ParamOld                    :Boolean                   ;
            PointFunc                   :Boolean                   ;
            FlagMoving                  :Boolean                   ;
            StepMoving                  :Extended                  ;
{-1-}   Constructor  Init                                          ;
{-2-}   Procedure    SetWindow(X1new,Y1new,X2new,Y2new:Integer)    ;
{-3-}   Procedure    Draw                                          ;
{-4-}   Procedure    DrawKoord                                     ;
{-6-}   Procedure    Show                                          ;
{-7-}   Procedure    Hide                                          ;
{-8-}   Procedure    Clear                                         ;
{-9-}   Procedure    CalcMasht                                     ;
{-10-}  Function     GetX (x:Extended)             :LongInt           ;
{-11-}  Function     GetY (y:Extended)             :LongInt           ;
{-12-}  Procedure    SetXYRange(t1,t2,t3,t4:Extended)              ;
{-13-}  Procedure    SetColor(c,f:Byte)                            ;
{-14-}  Function     GetRealX(x:LongInt)           :Extended       ;
{-15-}  Function     GetRealY(y:LongInt)           :Extended       ;
{-18-}  Procedure    PutPixel(x,y:Extended)                        ;
  End;

Implementation

Uses Graph;

Const

  TabLeft   = 20    ;
  TabRight  = 10    ;
  TabUp     = 40    ;
  TabDown   = 20    ;



Procedure Starting(k:Integer;j:integer;s0:String);
Begin

initgraph(k,j,s0);

End;


                   {-1-}

 Constructor Graphic.Init;
 Begin
   X1:=0; Y1:=0; X2:=GetMaxX; Y2:=GetMaxY;
   Vision:=False;
   Xmin:=0; Xmax:=1;
   Ymin:=-1; Ymax:=1;
   BackGround:=0; Color:=15;
   PointFunc:=False;
   ParamOld:=True;
{   FlagMoving:=False;
   StepMoving:=50;}
   CalcMasht;
 End;

                   {-2-}

 Procedure Graphic.SetWindow(X1new,Y1new,X2new,Y2new:Integer);
 Begin
   If ((X1<>X1new)Or(X2<>X2new)Or
      (Y1<>Y1New)Or(Y2<>Y2new))And
      (X2new-X1new>45)And(Y2new-Y1new>48) Then Begin
     X1:=X1new; X2:=X2new;
     Y1:=Y1new; Y2:=Y2new;
     ParamOld:=True;
     CalcMasht;
     Draw;
   End;
 End;

                   {-3-}

 Procedure Graphic.Draw;
 Var
   Param        :FillSettingsType;
   Cvet         :Byte            ;
 Begin
   If Vision And ParamOld Then Begin
     GetFillSettings(Param);
     Cvet:=GetColor;

     SetFillStyle(1,BackGround);
     Graph.SetColor(Color);
     Bar(x1,y1,x2,y2);

     Rectangle(x1,y1,x2,y2);

     DrawKoord;
{     DrawHelpInfo;}
     CalcMasht;

     Graph.SetColor(Cvet);
     SetFillStyle(Param.Pattern,Param.Color);

     ParamOld:=False;
   End;
 End;

                   {-4-}

 Procedure Graphic.DrawKoord;
 Var
   StepX,StepY      :Extended        ;
   i                :Byte            ;
   si0              :String          ;
 Begin
   CalcMasht;

   StepX:=(Xmax-Xmin)/10;
   StepY:=(Ymax-Ymin)/10;

   Line(x1+TabLeft,y2-TabDown+1,x1+TabLeft,y1+TabUp);
   Line(x1+TabLeft,y1+TabUp,x1+TabLeft+2,y1+TabUp+10);
   Line(x1+TabLeft,y1+TabUp,x1+TabLeft-2,y1+TabUp+10);
   Line(x1+TabLeft-2,y2-TabDown,x2-TabRight,y2-TabDown);
   Line(x2-TabRight,y2-TabDown,x2-TabRight-10,y2-TabDown-2);
   Line(x2-TabRight,y2-TabDown,x2-TabRight-10,y2-TabDown+2);

   For i:=1 to 10 do Begin
    If GridGraphik Then Begin
      MoveTo(GetX(Xmin+StepX*i),y2-TabDown+2);
      LineTo(GetX(Xmin+StepX*i),GetY(Ymax));
    End
    Else Begin
      MoveTo(GetX(Xmin+StepX*i),y2-TabDown+2);
      LineTo(GetX(Xmin+StepX*i),y2-TabDown-2);
    End;
    Str(i,si0);OutTextXY(GetX(Xmin+StepX*i)-3,y2-TabDown+2,si0);
    If GridGraphik Then Begin
      MoveTo(x1+TabLeft-2,GetY(Ymin+i*StepY));
      LineTo(GetX(Xmax),GetY(Ymin+i*StepY));
    End
    Else Begin
      MoveTo(x1+TabLeft-2,GetY(Ymin+i*StepY));
      LineTo(x1+TabLeft+2,GetY(Ymin+i*StepY));
    End;
    Str(i,si0);OutTextXY(x1+TabLeft-17,GetY(Ymin+i*StepY)-5,si0);
  End;

  OutTextXY(x2-TabRight,y2-TabDown+3,'X');
  OutTextXY(x1+TabLeft-17,y1+TabUp-2,'Y');
  OutTextXY(x1+TabLeft-17,y2-TabDown-3,'0');
  OutTextXY(x1+TabLeft-3,y2-TabDown+3,'0');

 End;

                   {-5-}

                   {-6-}

 Procedure Graphic.Show;
 Begin
   If Not(Vision) Then Begin
     ParamOld:=True;
     Vision:=True;
     Draw;
   End;
 End;

                   {-7-}

 Procedure Graphic.Hide;
 Begin
   Vision:=False;
 End;

                   {-8-}

 Procedure Graphic.Clear;
 Begin
   ParamOld:=True;
   Draw;
 End;

                   {-9-}

 Procedure Graphic.CalcMasht;
 Var
   Setka        :Extended   ;
 Begin
   Setka:=x2-x1-TabLeft-TabRight-25;
   MashtX:=Setka/(Xmax-Xmin);
   Setka:=y2-y1-TabUp-TabDown-20;
   MashtY:=Setka/(Ymax-Ymin);
   TabX:=x1+TabLeft; TabY:=y1+y2-TabDown-10*Round((y2-TabUp-TabDown-20-y1)/10);
   Seredina:=y1+y2-TabDown-10*Round((y2-TabUp-TabDown-20-y1)/10)+((y2-y1-TabUp-TabDown-20) div 2);
 End;


{ Procedure Graphic.CalcMasht;
 Var
   Setka        :Extended   ;
 Begin
   Setka:=x2-x1-45;
   MashtX:=Setka/(Xmax-Xmin);
   Setka:=y2-y1-48;
   MashtY:=Setka/(Ymax-Ymin);
   TabX:=x1+19; TabY:=y1+35;
   Seredina:=y1+36+((y2-y1-48) div 2);
 End;}

                   {-10-}

 Function Graphic.GetX(x:Extended):LongInt;
 Var
   Xnew      :Extended   ;
 Begin
   Xnew:=(x-Xmin)*MashtX+TabX;
   If (Xnew<x1)Or(Xnew>x2) Then Begin
     If Xnew<x1 Then GetX:=X1
     Else GetX:=X2;
   End
   Else GetX:=LongInt(Round(Xnew));
 End;

                   {-11-}

 Function Graphic.GetY(y:Extended):LongInt;
 Var
   Ynew      :Extended   ;
 Begin
   Ynew:=(y-Ymin)*MashtY+TabY;
   Ynew:=Ynew-(Ynew-Seredina)*2;
   If (Ynew<Y1)Or(Ynew>Y2) Then Begin
     If Ynew<Y1 Then GetY:=Y1
     Else GetY:=Y2;
   End
   Else Begin
     GetY:=LongInt(Round(Ynew));
   End;
 End;

                   {-12-}

 Procedure Graphic.SetXYRange(t1,t2,t3,t4:Extended);
 Begin
   If (Xmin<>t1)Or(Xmax<>t2)Or
      (Ymin<>t3)Or(Ymax<>t4) Then Begin
     ParamOld:=True;
     Xmin:=t1; Xmax:=t2;
     Ymin:=t3; Ymax:=t4;
     CalcMasht;
     Draw;
   End;
 End;

                   {-13-}

 Procedure Graphic.SetColor(c,f:Byte);
 Begin
   BackGround:=f;
   Color:=c;
   ParamOld:=True;
   Draw;
 End;

                   {-14-}

 Function Graphic.GetRealX(X:LongInt):Extended;
 Begin
   GetRealX:=(X-TabX)/MashtX+Xmin;
 End;

                   {-15-}

 Function Graphic.GetRealY(Y:LongInt):Extended;
 Var
   Ynew    :Extended    ;
 Begin
   Ynew:=y-(y-Seredina)*2;
   GetRealY:=(Ynew-TabY)/MashtY+Ymin;
 End;

                   {-16-}





                   {-18-}

 Procedure Graphic.PutPixel(x,y:Extended);
 Begin
{   If GetX(x)>=x2 Then Moving;}
   Graph.PutPixel(GetX(x),GetY(y),GetColor);
 End;

                   {-19-}



                   {-21-}



                   {-22-}

End.
