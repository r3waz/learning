Uses  CRT,GRAPH,DOS,Helpm,Math;
Label M1;
type   real = extended;
Var    masheps                                   : extended;
       x,y,minX,minY,maxX,maxY,sinY,dx,xmin      : extended;
       k,i,j                                     : integer;
       gr0                                       : graphic;
       x1, x2, y1, y2, xs, ys                    : extended;
       x0, y0, L1, fi, fimin, f0, xl, xr, fl, fr : extended;
       lres,xres                                 : extended;
       a0,b0,delta                               : real;
       mode                                      : char;

procedure initGraph0;
begin
  Starting(vga,vgahi,' ');
                    gr0.init;
          gr0.SetXYRange(minX,maxX,minY,maxY);

          gr0.SetWindow(0,0,GetMaxX,GetMaxY);
          gr0.Show;
          gr0.SetColor(15,1);
end;

Procedure Point(x,y:Extended;f:boolean);
Var r: Integer;
Begin
 r:=2;
  Circle(Gr0.GetX(x),Gr0.GetY(y),r);
 If f then SetFillStyle(1, 6)
      Else SetFillStyle(1, 6);
 { FloodFill(Gr0.GetX(x),Gr0.GetY(y),cyan); }
 Circle(Gr0.GetX(x),Gr0.GetY(y),r);
End;

function f(var x:extended):extended;

 begin
 f := sqrt(1+x*x)+exp(-2*x);
 end;

procedure meps;
 begin
  i:=0;
  masheps:=1;
  while 1 + masheps > 1
   do begin
   masheps := masheps/2;
   i:=i+1;
   end;
 end;

procedure dichotomy;
 begin writeln; write(' Dichotomy '); writeln;

 while lres > sqrt(masheps) do
  begin
  x1 := (a0+b0-delta)/2;
  x2 := (a0+b0+delta)/2;
  y1 := f(x1); y2 := f(x2);
  if y1 < y2 then
   b0 := x2
  else
   a0 := x1;
  j := j + 1;
  lres := abs(a0-b0);

  xres := (a0+b0)/2;
  writeln; write(' X min: '); write(xres); write(' Iterations: '); write(j);
  end;

  writeln;
 end;

procedure goldratio;
 begin    writeln; write(' Golden Ratio '); writeln;

 x1 := a0 + 2/(3+sqrt(5))*(b0-a0);
 x2 := a0 + 2/(1+sqrt(5))*(b0-a0);
 y1 := f(x1); y2 := f(x2);

 a0:=0; b0:=1.0;
 meps;
 j := 0;
 lres := abs(a0-b0);

 while lres > sqrt(masheps) do
  begin
  if f(x1) < f(x2) then begin
   b0 := x2;
   x2 := x1;
   y2 := y1;
   x1 := a0 + 2/(3+sqrt(5))*(b0-a0);
   y1 := f(x1);
   end
  else begin
   a0 := x1;
   x1 := x2;
   y1 := y2;
   x2 := a0 + 2/(1+sqrt(5))*(b0-a0);
   y2 := f(x2);
   end;
  j := j + 1;
  lres := abs(a0-b0);

  xres := (a0+b0)/2;
  writeln; write(' X min: '); write(xres); write(' Iterations: '); write(j);
  end;

  writeln;
 end;

procedure halfdiv;
 begin writeln; write(' Half-division '); writeln;
 a0:=0; b0:=1.0;
 meps;

 delta := sqrt(masheps)*0.5;
 j := 0;
 lres := abs(a0-b0);
 xs := (a0+b0)/2;

 while lres > sqrt(masheps) do
  begin
  x1 := a0 + lres/4;
  x2 := b0 - lres/4;
  y1 := f(x1); y2 := f(x2); ys:= f(xs);

  if y1 < ys then begin
   b0 := xs;
   xs := x1;
   end
   else begin
    if y2 < ys then
     begin
     a0 := xs;
     xs := x2;
     end
    else
     begin
     a0 := x1;
     b0 := x2;
     end;
   end;
  j := j + 1;
  lres := abs(a0-b0);

  xres := (a0+b0)/2;
  writeln; write(' X min: '); write(xres); write(' Iterations: '); write(j);
  end;

 writeln;
 end;


procedure polygonal;

 begin writeln; write(' Polygonal lines '); writeln;


 minX:=-0.25;
  maxX:=1.25;
  minY:=-3.5;
  maxY:=3;

  initGraph0;
  gr0.DrawKoord;
  dx:=(maxX-minX)/GetMaxX;
  SetColor(0);
  for k:=1 to GetMaxX do
  begin

    x:=minX+k*dx;
    y:=f(x);
    gr0.putPixel(x,y);

  end;

 a0:=0.0; b0:=1.0;
 meps;

 L1 := 6;
 j := 0;
 x0 := (1/(2*L1)) * (f(a0) - f(b0) + L1*(a0+b0));
 y0 := (1/(2)) * (f(a0) - f(b0) + L1*(a0-b0));
 fimin := y0;
 f0 := f(x0);
 fi := (1/2) * (f0 + fimin);
 Delta := (1/(2*L1)) * (f0-fimin);

 SetLineStyle(SolidLn, $C3, NormWidth);

 SetColor(7);
 line(Gr0.GetX(x0) , Gr0.GetY(y0) , Gr0.GetX(a0) , Gr0.GetY(f(a0)) );
  line(Gr0.GetX(x0) , Gr0.GetY(y0) , Gr0.GetX(b0) , Gr0.GetY(f(b0)) );

 while 2*L1*Delta > sqrt(masheps) do
  begin

   Delta := (1/(2*L1)) * (f0-fimin);

   xl := x0 - Delta;
   xr := x0 + Delta;

   f0 := f(x0);
   fi := (1/2) * (f0 + fimin);

   fl := f(xl);
   fr := f(xr);

   if fl < fr then begin

    if j < 8 then  begin
     setcolor(7);
     line(Gr0.GetX(xl) , Gr0.GetY(fimin) , Gr0.GetX(x0) , Gr0.GetY(f(x0)) ); end;

    x0 := xl
    end

   else begin

    if j < 8 then begin
     setcolor(7);
     line(Gr0.GetX(xr) , Gr0.GetY(fimin) , Gr0.GetX(x0) , Gr0.GetY(f(x0)) ); end;

    x0 := xr;
    end;

   fimin := fi;

  xres := x0;
  j := j + 1;

  writeln; write(' X min: '); write(xres); write(' Iterations: '); write(j); write('  |||  xl ', xl, ' xr ', xr, '  ', fimin);

  end;

  point(xres,f(xres),true);

 writeln;

 end;


begin                   { = YOUR = EXISTENCE = IS = VOID = HERE = }

 a0:=0.0; b0:=1.0;
 meps;

 delta := sqrt(masheps)*0.5;
 j := 0;
 lres := abs(a0-b0);

 readln(mode);

 if mode = 'd' then

  dichotomy;

 if mode = 'g' then

  goldratio;

 if mode = 'h' then

  halfdiv;

  minX:=0;
  maxX:=1;
  minY:=1;
  maxY:=2;

  initGraph0;
  gr0.DrawKoord;
  dx:=(maxX-minX)/GetMaxX;
  SetColor(cyan);
  for k:=1 to GetMaxX do
  begin

    x:=minX+k*dx;
    y:=f(x);
    gr0.putPixel(x,y);

  end;
  point(xres,f(xres),true);

 if mode = 'p' then

  polygonal;

 readln;

end.
