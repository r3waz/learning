int light;

void setup() {
  // put your setup code here, to run once:
  pinMode(2, OUTPUT);
  pinMode(A0, INPUT);
  //Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  light = 0;
  for (int i=0; i<1; ++i)
  {
    light += analogRead(A0);
  }
  light = light/1;
  //Serial.println(light);
  if(light<200)
  {
    digitalWrite(2, HIGH);
  }
  else
  {
    digitalWrite(2, LOW);
  }
  delay(100);
}
