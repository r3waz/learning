int svet;
bool danger;

void setup() {
  pinMode(A0, INPUT);
  pinMode(3, OUTPUT);
  Serial.begin(9600);
  danger = false;
}

void loop() {
  svet = analogRead(A0);
  if(svet<600)
  {
    danger = true;
  }
  if (danger)
      {
        for (int i=0; i<200; ++i)
        {
          tone(3, 2*i+200);
          delay(5);
        }
        for (int i=0; i<200; ++i)
        {
          tone (3, 600-2*i);
          delay(5);
        }
      }
  delay(10);
}

